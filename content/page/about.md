---
title: About me
subtitle:
comments: false
bigimg: [{src: "/img/fuzzies.png"}]
---

I'am a data scientist living in Boulder Colorado. This blog is meant to
be a collection of my thoughts on data science, tech, hiking, climbing,
and anything else that's on my mind.
